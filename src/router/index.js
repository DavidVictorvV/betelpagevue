import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../views/HomePage.vue";
import CampusPage from "../views/CampusPage.vue";
import DespreNoi from "../views/DespreNoi.vue";
import AcademicPage from "../views/AcademicPage.vue";
import InscrieriPage from "../views/InscrieriPage.vue";
import DocumentePage from "../views/DocumentePage.vue";
import StiriPage from "../views/StiriPage.vue";
import ContactPage from "../views/ContactPage.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Home",
      component: HomePage,
    },
    {
      path: "/campus",
      name: "Campus",
      component: CampusPage,
    },
    {
      path: "/despre-noi",
      name: "Despre noi",
      component: DespreNoi,
    },
    {
      path: "/academic",
      name: "Academic",
      component: AcademicPage,
    },
    {
      path: "/inscrieri",
      name: "Inscrieri",
      component: InscrieriPage,
    },
    {
      path: "/documente",
      name: "Documente",
      component: DocumentePage,
    },
    {
      path: "/stiri",
      name: "Stiri",
      component: StiriPage,
    },
    {
      path: "/contact",
      name: "Contact",
      component: ContactPage,
    },
  ],
});

export default router;
